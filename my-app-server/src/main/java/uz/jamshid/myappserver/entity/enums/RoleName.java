package uz.jamshid.myappserver.entity.enums;

public enum RoleName {
    ROLE_SUPERADMIN,
    ROLE_ADMIN,
    ROLE_AGENT,
    ROLE_CUSTOMER,
}
