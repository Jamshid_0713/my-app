package uz.jamshid.myappserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.jamshid.myappserver.entity.User;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    Optional<User> findByPhoneNumberOrEmail(String phoneNumber, String email);
}
