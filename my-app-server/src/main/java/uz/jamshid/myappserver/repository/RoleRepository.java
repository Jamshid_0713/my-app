package uz.jamshid.myappserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.jamshid.myappserver.entity.Role;
import uz.jamshid.myappserver.entity.enums.RoleName;

import java.util.List;

public interface RoleRepository extends JpaRepository<Role,Integer> {
    List<Role> findAllByNameIn(List<RoleName> name);
}
