package uz.jamshid.myappserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyAppServer {

    public static void main(String[] args) {
        SpringApplication.run(MyAppServer.class, args);
    }

}
