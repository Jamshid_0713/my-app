package uz.jamshid.myappserver.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.jamshid.myappserver.entity.User;
import uz.jamshid.myappserver.entity.enums.RoleName;
import uz.jamshid.myappserver.repository.RoleRepository;
import uz.jamshid.myappserver.repository.UserRepository;

import java.util.Arrays;

@Component
public class DataLoader implements CommandLineRunner {

    @Value("${spring.datasource.initialization-mode}")
    private String initialMode;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {
        if (initialMode.equals("always")) {
            userRepository.save(new User(
                    "123",
                    passwordEncoder.encode("123"),
                    "alisherabuhabiba@gmail.com",
                    "Alisher",
                    "Atadjanov",
                    roleRepository.findAllByNameIn(
                            Arrays.asList(RoleName.ROLE_ADMIN,
                                    RoleName.ROLE_SUPERADMIN)
                    )));
        }
    }
}
